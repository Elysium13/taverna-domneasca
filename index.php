<?php 
session_start(); 
  if (isset($_GET['logout'])) {
      unset($_SESSION['username']);
      session_destroy();
      setcookie("PHPSESSID","",time()-3600,"/");
  }
?>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />

    <title>Taverna Domnească</title>

    <link rel="stylesheet" href="css/Design.css"> 
    
    <div class="modalImg" id="MODALimg"></div>
    <img id="lamp-off" onclick="changeImage()" src="img/lamp_off.png" width="260" height="240" style="cursor:pointer">
    <img id="lamp-on" src="img/lamp_on.png" width="260" height="240">
    <div  id="text-image">Can you turn on the light,<br/> &emsp;&emsp;&emsp; please?</div>

</head>
<body>
    <div id="MODAL" class="modal"></div>
        <div class="content"> 
            <header class="site-header">
                <h1 class="site-title">Taverna Domnească</h1>
                <p class="site-description">Bine ați venit !</p>
                <div class="account-button" id="account-Button">
                    <div class="container"><a  id="acc-btn" class="btn effect01" target="_blank"><span>Contul meu</span></a></div>
                </div>
                <div>
    	            <p id="btn-logout-text">Welcome &nbsp;<strong><?php echo $_SESSION['username']; ?> </strong> </p>  
    	            <a href="index.php?logout='1'" style="color:red;"  id="btn-logout" > &nbsp;logout</a> 
                </div>
                <div class="main-nav" id="nav-buttons" align="center">
                    <div class="nav-item"><a class="nav-link" href="index.php">Home</a></div>
                    <div class="nav-item"><a class="nav-link" href="oferte.php">Oferte</a></div>
                    <div class="nav-item"><a class="nav-link" href="meniu.php">Meniu</a></div>
                    <div class="nav-item"><a class="nav-link" href="contact.php">Contact</a></div>  
                </div>
                <div  class='register-form'>
                    <div class='form-box'>
                        <div id="Close" class="close">&times;</div>
                        <div class="button-box">
                            <div id="slide-btn"></div>
                            <button type="button"class="toggle-btn" onclick="login()">Log in</button>
                            <button type="button"class="toggle-btn" onclick="register()">Register</button>
                        </div>
                        <div class="social-icons">
                            <img src="img/fb.png">
                            <img src="img/tw.png">
                            <img src="img/gp.png">
                        </div>
                        <form id="login" class="input-group"  method="post" action="login.php">
                            <p id="error-login"> Eroare login </p>
                            <input type="text" class="input-field" placeholder="User Id" required name="username">
                            <input type="password" class="input-field" placeholder="Enter Password" name="password" required>
                            <input type="checkbox" class="check-box"><spans>Remember Password</spans>
                            <button type="submit" class="submit-btn" name="login_user">Log in</button>
                        </form>  
                        <form id="register" class="input-group" method="post" action="register.php">
                            <p id="error-register"> Eroare Username/Email </p>
                            <input type="text" class="input-field" placeholder="User Id" required name="username">
                            <input type="email" class="input-field" placeholder="Email Id" required name="email"> 
                            <input type="password" class="input-field" placeholder="Enter Password" required name="password">
                            <input type="checkbox" class="check-box" id="check-box"><spans>I agree to the terms & conditions</spans>
                            <button type="submit" class="submit-btn" name="register_button" id="register_button">Register</button>
                        </form>
                    </div>
                </div>
            </header>

            <div class="main-content">
                <header></header>
                <p>Taverna Domnească este restaurantul în care te vei simți întotdeauna bine primit, situat în imediata apropiere de centrul istoric al orașului.<br>
                    Venim în întampinarea dumeneavostră cu un concept nou: Bucătărie Contemporană Românească. Bucătarii noștri gătesc zilnic, special pentru cei care ne calcă pragul, preparate tradiționale și contemporane dupa rețete de excepție, iar aromele inconfundabile vă vor satisface în mod cert cele mai exigente gusturi. <br><br>
                    Cei care obișnuiesc să ne fie oaspeți știu deja aceste lucruri iar cei care însă au ajuns din întamplare sau din curiozitate au fost încântați să ne cunoască.<br>  <br><br> <br>    <br> <br>   
                    <img src="img/poza2.jpg" id="tavern-image">
                </p>
            </div>
        </div>
       

            <div align="center">
                <td><marquee behavior="scroll"  id="text-footer" width=800; height=50;  direction="left" ><strong>© 2020   Sirbu Alexandru-Paul </strong> </marquee><td>
            </div>

<script  src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<script src="js/bootstrap.min.js"></script>

<script src="js/script.js"></script>

</body>
</html>