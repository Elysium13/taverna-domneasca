var x = document.getElementById("login");
var y = document.getElementById("register");
var z = document.getElementById("slide-btn");

function register() {
  x.style.left = "-400px";
  y.style.left = "50px";
  z.style.left = "110px";
}
function login() {
  x.style.left = "50px";
  y.style.left = "450px";
  z.style.left = "0";
}

document.getElementById("acc-btn").addEventListener("click", function () {
  document.querySelector(
    "body > div.content > header > div.register-form"
  ).style.display = "flex";
  document.querySelector("body > div.modal").style.display = "flex";
});

document.getElementById("MODAL").addEventListener("click", function () {
  document.querySelector(
    "body > div.content > header > div.register-form"
  ).style.display = "none";
  document.querySelector("body > div.modal").style.display = "none";
  document.getElementById("error-register").style.display = "none";
  document.getElementById("error-login").style.display = "none";
});

document.getElementById("Close").addEventListener("click", function () {
  document.querySelector(
    "body > div.content > header > div.register-form"
  ).style.display = "none";
  document.querySelector("body > div.modal").style.display = "none";
  document.getElementById("error-register").style.display = "none";
  document.getElementById("error-login").style.display = "none";
});

function errorUser() {
  document.getElementById("register_button").disabled = true;

  var error = localStorage.getItem("error_username");
  var error_msg = localStorage.getItem("error_msg");
  var error_msg_log = localStorage.getItem("error_msg_log");

  if (error == "True") {
    register();
    document.querySelector(
      "body > div.content > header > div.register-form"
    ).style.display = "flex";
    document.querySelector("body > div.modal").style.display = "flex";
    localStorage.setItem("error_username", "False");

    if (error_msg == "User") {
      document.getElementById("error-register").innerHTML =
        "Username already exists,please try again";
      document.getElementById("error-register").style.display = "flex";
    } else {
      document.getElementById("error-register").innerHTML =
        "Email already exists,please try again";
      document.getElementById("error-register").style.display = "flex";
    }
  }

  if (error_msg_log == "True") {
    login();
    document.querySelector(
      "body > div.content > header > div.register-form"
    ).style.display = "flex";
    document.querySelector("body > div.modal").style.display = "flex";
    localStorage.setItem("error_msg_log", "False");
    document.getElementById("error-login").innerHTML =
      "Wrong username/password combination";
    document.getElementById("error-login").style.display = "flex";
  }
}

errorUser();

document.getElementById("check-box").addEventListener("click", function () {
  if (document.getElementById("check-box").checked) {
    document.getElementById("register_button").disabled = false;
  } else {
    document.getElementById("register_button").disabled = true;
  }
});

function button_hidden() {
  var error = localStorage.getItem("button-hidden");
  if (error == "True") {
    document.getElementById("account-Button").style.display = "none";
    document.getElementById("btn-logout").style.display = "flex";
    document.getElementById("btn-logout-text").style.display = "flex";
  }
}

button_hidden();

document.getElementById("btn-logout").addEventListener("click", function () {
  localStorage.setItem("button-hidden", "False");
});

window.onload = function () {
  if (sessionStorage.getItem("hide_Img") == null) {
    sessionStorage.setItem("hide_Img", "Off");
  } else if (sessionStorage.getItem("hide_Img") == "On") {
    document.getElementById("lamp-on").style.display = "flex";
    document.getElementById("text-image").style.display = "none";
    document.getElementById("MODALimg").style.display = "none";
  }
};

function changeImage() {
  let hideImg = sessionStorage.getItem("hide_Img");
  let image = document.getElementById("lamp-off");
  if (hideImg == "Off") {
    if (image.src.match("img/lamp_off.png")) {
      $("#lamp-on").fadeIn(6400);
      $("#MODALimg").fadeOut(1800);
      document.getElementById("text-image").style.display = "none";
      sessionStorage.setItem("hide_Img", "On");
      console.log(hideImg);
    }
  }
}
